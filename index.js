const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Allows us to access Routes
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

// Setting the port to be used
const port = process.env.PORT || 4000;

const app = express();

// Connecting MongoDB
mongoose.connect("mongodb+srv://admin:admin123@batch204-likristoffer.rqyug4v.mongodb.net/s42-s46?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", () => console.error.bind(console, "Error!"));
db.once("open", () => console.log("Now connected to MongoDB Atlas!"));

// Allows all resources to access our backend application
app.use(cors());
app.use(express.json());

// localhost
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
});