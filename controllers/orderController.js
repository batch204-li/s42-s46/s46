const Order = require("../models/Order");
const bcrypt = require("bcrypt");

// const auth = require("../auth");


// Checkout
module.exports.checkout = (userId, cart) => {
	// console.log(cart.products[0].productId);
	
	return Order.findById(userId).then(order => {
		let newOrder = new Order({
			userId: userId,
			products: cart.products,
		})

		// console.log(newOrder)
		return newOrder.save().then((order, error) => {
			if(error) {
				return false;
			}else {
				return true;
			}
		});

	})
}

// View Orders
module.exports.viewOrders = (userId) => {
	return Order.find({userId: userId}).then(user => {
		if(user === null) {
			return false
		}else {
			return user;
		}
	})
}