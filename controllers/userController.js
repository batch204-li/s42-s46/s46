const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");
// const ObjectId = require('mongodb').ObjectId



// Check email exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(
		result => {
			console.log(result)
			if(result.length > 0) {
				return true;
			}else {
				return false;
			}
		})
}
// Controller for User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	});


	return newUser.save().then((user, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
}

// User Login/Athentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// If the password match/result of the code above is true
			if(isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			} else {
				return false;
			}
		}
	});
}


// Get profile
module.exports.getProfile = (data) => {
	
	return User.findById(data.id).then(result => {
		result.password = "";

		return result;
	})
}

// Add to Cart
module.exports.addToCart = (userData, data) => {

	console.log(data.productId)

	return Product.findById(data.productId).then(result => {
		return User.findById(userData.id).then(user => {
			if(user === null) {
				return false
			}else {
				// console.log(result.quantity)
				user.cart.unshift(
				{
					productName: result.productName,
					productId: data.productId,
					price: result.price,
					quantity: data.quantity
				});
				console.log(user.cart)
				const currentOrder = user.cart[0]
				// console.log(result.orders)
				return user.save().then((updatedUser, error) => {
					if(error) {
						return false
					}else {
						const currentOrder = updatedUser.cart[0];

						result.orders.unshift(
						{
							orderId: currentOrder._id,
							_id: null
						})
						result.save()

						return true
					}
				});
			}
		})
	})
}

// View Cart
module.exports.viewCart = (userData) => {

	// console.log(userData)
	return User.findById(userData.id).then(result => {
		if(result) {
			return result;
		}else {
			return false
		}
	})
}

// Remove from Cart
module.exports.removerFromCart = (userData, reqBody) => {

	// console.log(userData)
	// console.log(reqBody.id)
	return User.findById(userData.id).then(user => {
		// console.log(user.cart[reqBody.index].id)
		const orderId = user.cart[reqBody.index].id
		user.cart.splice((reqBody.index), 1)
		console.log(user.cart)

		user.save()
		// console.log(reqBody.id)
		return Product.findById(reqBody.id).then(product => {
			// console.log(product)
			let productOrderIndex = 0;
			for(let i = 0; i < (product.orders.length); i++) {
				if((product.orders[i].orderId) === orderId) {
					productOrderId = i
				}
			}
			// console.log(product.orders.length)
			// console.log(productOrderIndex)
			product.orders.splice(productOrderIndex, 1)
			console.log(product.orders)

			product.save()

			return true
		})

	});

		// return map[0]

}