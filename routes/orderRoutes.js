const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require('../auth.js');


// Check out
router.post("/checkout", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === true){
		res.send(false);
	}else {
		const userId = auth.decode(req.headers.authorization).id;

		orderController.checkout(userId, req.body).then(resultFromController => res.send(resultFromController));
	}
});

// Get users order
router.get("/myOrders", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === true){
		res.send(false);
	}else {
		const userId = auth.decode(req.headers.authorization).id;

		orderController.viewOrders(userId).then(resultFromController => res.send(resultFromController));
	}
});

module.exports = router;