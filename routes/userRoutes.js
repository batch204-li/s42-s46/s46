const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

// Check email exists
router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(
		resultFromController => res.send(resultFromController));
});

// Route to User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route to User login/Athentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route to get user info
router.get("/details", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({id: userData.id}).then(
		resultFromController => res.send(resultFromController));
});

// Add to cart
router.post("/addToCart", auth.verify, (req, res) => {

	if(auth.decode(req.headers.authorization).isAdmin === true) {
		res.send(false);
	}else {
		let userData = auth.decode(req.headers.authorization)

		userController.addToCart(userData, req.body).then(resultFromController => res.send(resultFromController));
	}

});

// Cart Details
router.get("/cart", auth.verify, (req, res) => {

	if(auth.decode(req.headers.authorization).isAdmin === true) {
		res.send(false);
	}else {
		let userData = auth.decode(req.headers.authorization)

		userController.viewCart(userData).then(resultFromController => res.send(resultFromController));
	}

});

// Remove item on cart
router.delete("/cart", auth.verify, (req, res) => {

	if(auth.decode(req.headers.authorization).isAdmin === true) {
		res.send(false);
	}else {
		let userData = auth.decode(req.headers.authorization)

		userController.removerFromCart(userData, req.body).then(resultFromController => res.send(resultFromController));
	}

});

module.exports = router;