const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	productName: {
		type: String,
		required: [true, "This is required!"]
	},
	description: {
		type: String,
		required: [true, "This is required!"]
	},
	price: {
		type: Number,
		required: [true, "This is required!"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [
		{
			orderId: {
				type: String,
				required: [true, "This is required"]
			}
		}
	]
});

module.exports = mongoose.model("Product", productSchema);