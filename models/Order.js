const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "UserId required"]
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, "productId required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity required"]
			}
		}
	],
	totalAmount: {
		type: Number
	},
	puchasedOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model("Order", orderSchema);