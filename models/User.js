const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "This is required!"]
	},
	lastName: {
		type: String,
		required: [true, "This is required!"]
	},
	email: {
		type: String,
		required: [true, "This is required!"]
	},
	password: {
		type: String,
		required: [true, "This is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: Number,
		required: [true, "This is required!"]
	},
	cart: [
		{
			productName: {
				type: String,
				required: [true, "This is required"]
			},
			productId: {
				type: String,
				required: [true, "This is required"]
			},
			price: {
				type: Number,
				required: [true, "This is required"]
			},
			quantity: {
				type: Number,
				required: [true, "This is required"]
			}
		}
	]
});

module.exports = mongoose.model("User", userSchema);